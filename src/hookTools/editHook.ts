import {KeyboardEvent, useState} from "react";

type editHookProps = {
    defaultTitle:string;
    onEnter_ValidInput:()=>void;
    onEnter_EmptyInput?:()=>void;
}

export const EditHook = ({defaultTitle, onEnter_ValidInput, onEnter_EmptyInput}:editHookProps) => {

    const [editedTitle, setEditedTitle] = useState(defaultTitle);
    const [editing, setEditing] = useState(false);

    const HandleKeyUp = (event:KeyboardEvent<HTMLInputElement>) => {
        if (event.code === 'Enter') {
            if (editedTitle.trim().length>0){
                onEnter_ValidInput();
            }
            else {
                if(onEnter_EmptyInput){
                    onEnter_EmptyInput();
                }
            }
        }
        if (event.code === 'Escape') {
            cancelEdit()
        }
    }

    const cancelEdit = () => {
        setEditedTitle(defaultTitle);
        setEditing(false);
    }

    return{editedTitle, setEditedTitle, editing, setEditing, HandleKeyUp, handleCancelEdit: cancelEdit}
}