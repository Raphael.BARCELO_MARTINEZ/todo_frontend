import {useCallback, useState} from "react";
import {Todo} from "../model/models";

export const TodoHooks = () => {

    const [todos, setTodos] = useState<Todo[]>([]);

    const addNewTodo = useCallback((title:string): void => {
        const randomId:number = Math.floor((Math.random() * 10000));
        setTodos(state_todos => [
            ...state_todos,
            {id: randomId, title: title, completed: false},
        ]);
    },[setTodos]);

    const update = useCallback((id: number, title?:string, completed?:boolean): void => {
        setTodos((todos) => {
            return todos.map((todo) => {
                if (todo.id === id) {
                    title ??= todo.title;
                    completed ??= todo.completed;
                    return {...todo, title, completed}
                }
                return todo;
            })
        });
    },[setTodos]);

    const toggleCompleted_All = useCallback((displayedTodos:Todo[]): void => {
        setTodos((todos) => {
            return todos.map((todo) => {
                if (displayedTodos.includes(todo)) {
                    return {...todo, completed: !todo.completed}
                }
                return todo;
            })
        });
    },[setTodos]);

    const destroy = useCallback((id: number): void => {
        setTodos(todos => todos.filter((todo) => (todo.id !== id)))
    },[setTodos]);

    const clearCompleted = useCallback((): void => {
        setTodos(todos => todos.filter((todo) => !todo.completed))
    },[setTodos]);

    return{todos, addNewTodo, update, toggleCompleted_All, destroy, clearCompleted}
}