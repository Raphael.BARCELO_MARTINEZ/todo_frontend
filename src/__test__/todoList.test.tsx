import React from "react";
import '@testing-library/jest-dom';
import {render, screen} from "@testing-library/react"
import userEvent from  "@testing-library/user-event"
import List from "../Components/list";
import {Todo} from "../model/models";

describe("todo list component should", () => {
    it("call onToggleCompleted_All when checkbox is clicked", async () => {
        const todos:Todo[] = []
        const mockOnDestroy = jest.fn();
        const mockOnToggleCompleted_All = jest.fn();
        const mockOnUpdate = jest.fn();
        render(
            <List
                todos={todos} onDestroy={mockOnDestroy}
                onToggleCompleted_All={mockOnToggleCompleted_All}
                onUpdate={mockOnUpdate}
            />
        )

        userEvent.click(await screen.findByRole('checkbox'))

        expect(mockOnToggleCompleted_All).toHaveBeenCalledTimes(1)
    })
})