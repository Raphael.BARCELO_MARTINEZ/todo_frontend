import React from "react";
import '@testing-library/jest-dom';
import {render, screen} from "@testing-library/react"
import userEvent from  "@testing-library/user-event"
import TodoCreate from "../Components/todoCreate";

describe("todo create component should", () => {
    it("notify parent component when creating a todo with a valid title", async () => {
        const mockOnTodoCreate = jest.fn();
        render(<TodoCreate onTodoCreate={mockOnTodoCreate}/>)

        userEvent.type(await screen.findByRole('textbox'), "title{enter}")

        expect(mockOnTodoCreate).toHaveBeenCalledTimes(1)
        expect(mockOnTodoCreate).toHaveBeenNthCalledWith(1, "title")
    })

    it.each`
    inputs                 | testLabel
    ${'{enter}'}           | ${'empty'}
    ${'   {enter}'}        | ${'blank with spaces'}
    ${'{tab}{tab}{enter}'} | ${'blank with tabs'}
  `
    ("not notify parent component when creating a todo with an invalid $testlabel title", async ({inputs}) => {
        const mockOnTodoCreate = jest.fn();
        render(<TodoCreate onTodoCreate={mockOnTodoCreate}/>)

        await userEvent.type(await screen.findByRole('textbox'), inputs)

        expect(mockOnTodoCreate).not.toBeCalled()
    })
})