import React from "react";
import '@testing-library/jest-dom';
import {render, screen} from "@testing-library/react"
import Footer from "../Components/footer";
import { MemoryRouter, Routes, Route} from "react-router-dom";
import userEvent from "@testing-library/user-event";

describe("footer component should", () => {

    it("have called onClearCompleted after clicking on button", async () => {
        const numberOfTodos = 0;
        const areAnyTodoCompleted = true;
        const onClearCompleted = jest.fn();

        render(
                <MemoryRouter initialEntries={["/"]}>
                    <Routes>
                        <Route path="/" element={<Footer numberOfTodos={numberOfTodos}
                                                         areAnyTodoCompleted={areAnyTodoCompleted}
                                                         clearCompleted={onClearCompleted}/>} />
                    </Routes>
                </MemoryRouter>
            );

        expect(await screen.findByRole('button')).toBeInTheDocument()
        userEvent.click(await screen.findByRole('button'));
        expect(onClearCompleted).toHaveBeenCalledTimes(1);
    });

    const itemsLeftDisplayPredicate = (expectedNumberOfTodos: string, expectedItemLabel: string) => {
        return (content: string, element: Element | null) => {
            return (
                content === expectedItemLabel && ( // eslint-disable-next-line testing-library/no-node-access
                element?.parentNode?.textContent?.startsWith(expectedNumberOfTodos) ?? false
                )
            );
        };
    };

    it.each`
    numberOfTodos  | itemLabel 
    ${'0'}         | ${'items'}
    ${'1'}         | ${'item'}
    ${'2'}         | ${'items'}
    `(
        'should display $numberOfTodos $itemLabel left',
        async ({
                   numberOfTodos,
                   itemLabel
               }) => {

            const areAnyTodoCompleted = true;
            const onClearCompleted = jest.fn();

            render(
                <MemoryRouter initialEntries={["/"]}>
                    <Routes>
                        <Route path="/" element={<Footer numberOfTodos={numberOfTodos}
                                                         areAnyTodoCompleted={areAnyTodoCompleted}
                                                         clearCompleted={onClearCompleted}/>} />
                    </Routes>
                </MemoryRouter>
            );

            expect(
                await screen.findByText(
                    itemsLeftDisplayPredicate(
                        numberOfTodos,
                        itemLabel
                    )
                )
            ).toBeInTheDocument();
        }
    );
})