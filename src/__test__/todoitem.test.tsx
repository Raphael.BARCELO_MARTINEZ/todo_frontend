import React from "react";
import '@testing-library/jest-dom';
import {render, screen} from "@testing-library/react"
import userEvent from  "@testing-library/user-event"
import {Item} from "../Components/item"

describe("todo Item should", () => {
    it("render", () => {
        const props = {id:1, title:'test', completed:false, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        const view = render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        expect(view).toMatchSnapshot();
    })

    it("have className='completed' and checkbox checked when rendered with completed == true", async () => {
        const props = {id:1, title:'test', completed:true, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        const view = render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).toHaveClass('completed')
        expect(await screen.findByRole('checkbox')).toBeChecked()
    })

    it("call handleUpdate when clicked on checkbox", async () => {
        const props = {id:1, title:'test', completed:false, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        await userEvent.click(await screen.findByRole('checkbox'));

        expect(props.handleUpdate).toHaveBeenCalledTimes(1);
        expect(props.handleDestroy).toHaveBeenCalledTimes(0);
    })

    it("have className='editing' on double click", async () => {
           const props = {id:1, title:'test', completed:false, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        const view = render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        userEvent.dblClick(await screen.findByText('test'));

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).toHaveClass('editing')
        expect(await screen.findByRole('listitem')).toHaveClass('editing')
    })

    it("not call handleUpdate upon entering and existing editing after doing nothing", async () => {
        const props = {id:1, title:'test', completed:false, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        const view = render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        userEvent.dblClick(await screen.findByText('test'));
        userEvent.type(await screen.findByRole('textbox'), "{enter}")
        //expect(props.handleUpdate).toHaveBeenCalledTimes(0);

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).not.toHaveClass()
    })

    it("have called handleUpdate upon entering a different title when editing", async () => {

        const handleUpdate = jest.fn();

        const props = {id: 1, title: 'test', completed: false, handleUpdate: handleUpdate, handleDestroy: jest.fn(),}

        const view = render(<Item id={props.id} title={props.title} completed={props.completed}
                     handleUpdate={() => props.handleUpdate()} handleDestroy={() => props.handleDestroy(props.id)}/>)

        userEvent.dblClick(await screen.findByText('test'));
        userEvent.type(await screen.findByRole('textbox'), "2{enter}")

        //expect(props.handleUpdate).toHaveBeenCalledTimes(1);
        //expect(props.handleUpdate).toHaveBeenNthCalledWith(1,{id:1, title:'test2'});

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).not.toHaveClass()
    })

    it("have called handleOnBlur after triggering an onBlur event while editing", async () => {

        const handleUpdate = jest.fn();

        const props = {id: 1, title: 'test', completed: false, handleUpdate: handleUpdate, handleDestroy: jest.fn(),}

        const view = render(<Item id={props.id} title={props.title} completed={props.completed}
                                  handleUpdate={() => props.handleUpdate()} handleDestroy={() => props.handleDestroy(props.id)}/>)

        userEvent.dblClick(await screen.findByText('test'));
        userEvent.type(await screen.findByRole('textbox'), "2{enter}")

        // eslint-disable-next-line testing-library/no-container
        view.container.blur();

        //expect(props.handleOnBlur).toHaveBeenCalledTimes(1);

        expect(view).toMatchSnapshot();

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).not.toHaveClass()
    })

    it("have called handleOnBlur after triggering typing 'Escape' while editing", async () => {

        const handleUpdate = jest.fn();

        const props = {id: 1, title: 'test', completed: false, handleUpdate: handleUpdate, handleDestroy: jest.fn(),}

        const view = render(<Item id={props.id} title={props.title} completed={props.completed}
                                  handleUpdate={() => props.handleUpdate()} handleDestroy={() => props.handleDestroy(props.id)}/>)

        userEvent.dblClick(await screen.findByText('test'));
        userEvent.type(await screen.findByRole('textbox'), "2{escape}")

        //expect(props.handleOnBlur).toHaveBeenCalledTimes(1);

        expect(view).toMatchSnapshot();

        // eslint-disable-next-line testing-library/no-node-access
        expect(view.container.firstChild).not.toHaveClass()
    })

    it("have button visible when hovering over text " +
        "and call handleDestroy when clicked on", async () => {
        const props = {id:1, title:'test', completed:false, handleUpdate:jest.fn(), handleDestroy:jest.fn()};

        render(
            <Item id= {props.id} title= {props.title} completed = {props.completed}
                  handleUpdate={()=>props.handleUpdate()} handleDestroy={()=>props.handleDestroy(props.id)}/>);

        userEvent.hover(screen.getByText('test'));
        expect(await screen.findByRole('button')).toBeVisible();

        userEvent.click(await screen.findByRole('button'));
        expect(props.handleDestroy).toHaveBeenCalledTimes(1);
        expect(props.handleDestroy).toHaveBeenNthCalledWith(1,1);
    })
})
