import React from "react";
import {EditHook} from "../hookTools/editHook";

type TodoItemProps = {
    id:number;
    title:string;
    completed:boolean;
    handleUpdate:(id: number, title?:string, completed?:boolean)=>void;
    handleDestroy:()=>void;
}

export const Item = ({id, title, completed, handleUpdate, handleDestroy}:TodoItemProps) => {

    const {editedTitle, setEditedTitle, editing, setEditing, HandleKeyUp, handleCancelEdit} = EditHook({
            defaultTitle:title,
            onEnter_ValidInput:()=>{
                handleUpdate(id, editedTitle);
                setEditing(false);
                },
            onEnter_EmptyInput:handleDestroy })

    const className = (editing ? 'editing' : completed ?'completed':'')

    return (
        <li className={className}>
            {(className !== 'editing') &&
                <div className="view">
                    <input className="toggle"
                           type="checkbox"
                           onChange={() => handleUpdate(id, undefined, !completed)}
                           checked={completed}/>
                    <label onDoubleClick={()=>setEditing(true)} > {title} </label>
                    <button className="destroy" onClick={handleDestroy}/>
                </div>
            }
            {(className === 'editing') &&
                <input
                    className="edit"
                    value={editedTitle}
                    autoFocus
                    onChange={(event)=> {setEditedTitle(event.target.value)}}
                    onKeyUp={HandleKeyUp}
                    onBlur={handleCancelEdit}
                />
            }
        </li>
    );
}