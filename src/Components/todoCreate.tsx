import React from 'react';
import {EditHook} from "../hookTools/editHook";

type TodoCreateProps = {
    onTodoCreate:(title:string)=>void;
}

const TodoCreate = ({onTodoCreate}: TodoCreateProps) => {

    const {editedTitle, setEditedTitle, HandleKeyUp} = EditHook({
        defaultTitle:'',
        onEnter_ValidInput:()=> {
            onTodoCreate(editedTitle);
            setEditedTitle("");
            },
        onEnter_EmptyInput:undefined
    })

    return (
        <input value={editedTitle}
               className={'new-todo'}
               placeholder={'What needs to be done?'}
               autoFocus
               onChange={(event) => { setEditedTitle(event.target.value) }}
               onKeyUp={HandleKeyUp}
        />
    );
}
export default TodoCreate;