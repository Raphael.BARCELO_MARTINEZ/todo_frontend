import React from 'react';
import {Item} from "./item";
import {Todo} from "../model/models";

type TodoListProps = {
    todos:Todo[];
    onUpdate:(id: number, title?:string, completed?:boolean)=>void;
    onToggleCompleted_All:()=>void
    onDestroy:(id:number)=>void;
}

export const List = ({todos, onUpdate, onToggleCompleted_All, onDestroy}:TodoListProps) => {

    const handleDestroy = (id:number) => {
        onDestroy(id)
    }

    const rows = todos.map(({id,title, completed}) => {
        return (
                <Item
                    key={id}
                    id= {id}
                    title= {title}
                    completed = {completed}
                    handleUpdate={onUpdate}
                    handleDestroy={()=>handleDestroy(id)}
                />
        );
    });

    return (
        <section className="main">
            <input id="toggle-all" className="toggle-all" type="checkbox" onChange={onToggleCompleted_All}/>
            <label htmlFor="toggle-all">Mark all as complete</label>
            <ul className = "todo-list" >
                {rows}
            </ul>
        </section>
    )
}
export default List;