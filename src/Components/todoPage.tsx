import {TodoHooks} from "../hookTools/todoHooks";
import TodoCreate from "./todoCreate";
import {List} from './list';
import {Footer} from './footer';
import {Filter} from "../model/filter.enum";

type TodoPageProps = {
    filter:Filter
}

const TodoPage = ({filter}:TodoPageProps) => {

    const {todos, addNewTodo, update, toggleCompleted_All, destroy, clearCompleted} = TodoHooks()

    const areAnyTodoCompleted = todos.findIndex((todo) => todo.completed) > -1

    const displayedTodos = todos.filter((todo)=>{
        switch (filter){
            case Filter.ALL:
                return true;
            case Filter.ACTIVE:
                return !todo.completed
            case Filter.COMPLETED:
                return todo.completed
            default:
                return true;
        }
    })

    const onAddNewTodo = (title: string) => {
        addNewTodo(title)
    }

    const onUpdate = (id: number, title?:string, completed?:boolean) => {
        update(id, title, completed)
    }

    const onToggleCompleted_All = () => {
        toggleCompleted_All(displayedTodos)
    }

    const onDestroy = (id: number) => {
        destroy(id);
    }

    const onClearCompleted = () => {
        clearCompleted()
    }

    return (
        <section className="todoapp">
            <header className="header">
                <h1>todos</h1>
                <TodoCreate onTodoCreate={(value) => onAddNewTodo(value)}/>
            </header>
            {todos.length > 0 && (
                <>
                    <List
                        todos={displayedTodos}
                        onUpdate={onUpdate}
                        onToggleCompleted_All={onToggleCompleted_All}
                        onDestroy={onDestroy}
                    />
                    <Footer
                        numberOfTodos={displayedTodos.length}
                        areAnyTodoCompleted={areAnyTodoCompleted}
                        clearCompleted={onClearCompleted}
                    />
                </>
            )}
        </section>
    );
}

export default TodoPage;